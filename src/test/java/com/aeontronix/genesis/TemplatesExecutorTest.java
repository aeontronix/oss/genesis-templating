package com.aeontronix.genesis;

import com.aeontronix.commons.file.FileUtils;
import com.aeontronix.commons.file.TempDir;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.Objects;

import static junit.framework.TestCase.assertTrue;
import static junit.framework.TestCase.fail;

public class TemplatesExecutorTest {
    public static final String TEST_ONE_FILE = "/test-one-file.json";
    public static final String TEST_ONE_FILE_WITH_INPUT = "/test-one-file-with-inputs.json";
    private TempDir tmpDir;

    @Before
    public void init() throws IOException, InvalidTemplateException {
        tmpDir = TempDir.createMavenTmpDir();
    }

    @After
    public void cleanup() {
        tmpDir.close();
    }

    @Test
    public void testValNoInput() throws Exception {
        executeWithVar(TEST_ONE_FILE, "testval", "someval");
        assertOneFile("# someval");
    }

    @Test
    public void testValConditional() throws Exception {
        executeWithVar("/test-conditional.json", "val", "yes");
        assertOneFile("# yes - foo");
    }

    @Test
    public void testValConditionalMissing() throws Exception {
        executeWithVar("/test-conditional.json", "val", "foo");
        assertOneFile("# foo - MISSING");
    }

    @Test
    public void testValOldFilesGone() throws Exception {
        FileUtils.write(new File(tmpDir,"grab.txt"), "CCC" );
        executeWithVar(TEST_ONE_FILE, "testval", "someval");
        assertOneFile("# someval");
    }

    @Test
    public void testValNoInputYaml() throws Exception {
        executeWithVar("/test-one-file.yml", "testval", "someval");
        assertOneFile("#X someval");
    }

    @Test
    public void testValWithInput() throws Exception {
        executeWithVar(TEST_ONE_FILE_WITH_INPUT, "testval", "someval");
        assertOneFile("# someval");
    }

    @Test
    public void testValWithTwoInputs() throws Exception {
        TemplateExecutor executor = createExecutor("/test-one-file-with-2-inputs.json");
        try {
            executor.execute();
            fail("didn't throw VariableMissingException");
        } catch (VariableMissingException e) {
            assertTrue(e.getMessage().contains("i1"));
        }
        try {
            executor.setVariable("i1", "foo");
            executor.execute();
            fail("didn't throw VariableMissingException");
        } catch (VariableMissingException e) {
            assertTrue(e.getMessage().contains("i2"));
        }
        executor.setVariable("i2", "bar");
        executor.execute();
        assertOneFile("# foo - bar");
    }

    @Test
    public void testValWithTwoInputsDefault() throws Exception {
        TemplateExecutor executor = createExecutor("/test-one-file-with-2-inputs-default.json");
        executor.setVariable("i1", "foo");
        executor.execute();
        assertOneFile("# foo - foo");
    }

    @Test(expected = TemplateExecutionException.class)
    public void testVarNotSet() throws Exception {
        TemplateExecutor executor = createExecutor(TEST_ONE_FILE);
        executor.execute();
    }

    @Test(expected = VariableMissingException.class)
    public void testWithInputVarNotSet() throws Exception {
        TemplateExecutor executor = createExecutor(TEST_ONE_FILE_WITH_INPUT);
        executor.execute();
    }

    @Test
    public void testWithOptionVar() throws Exception {
        executeWithVar("/test-one-file-with-inputs-options.json", "testval", "op1");
        assertOneFile("# op1");
    }

    @Test(expected = InvalidVariableException.class)
    public void testWithInvalidOptionVar() throws Exception {
        executeWithVar("/test-one-file-with-inputs-options.json", "testval", "someval");
    }

    @Test
    public void testClasspath() throws Exception {
        executeWithVar("classpath://", "testval", "XX");
        assertFileCount(2);
        assertFile("README.md", "XX");
        assertFile("foo.txt", "vroom");
    }

    @Test
    public void testSubfolder() throws Exception {
        final Template template = Template.createFromClasspath("/subfolder/","test-subfolder.json");
        final TemplateExecutor templateExecutor = new TemplateExecutor(template, tmpDir);
        templateExecutor.execute();
        assertFileCount(1);
        assertFile("foo.txt", "bar");
    }

    private TemplateExecutor executeWithVar(String name, String myval, String foo) throws Exception {
        TemplateExecutor executor = createExecutor(getTestFileUrl(name));
        executor.setVariable(myval, foo);
        executor.execute();
        return executor;
    }

    private TemplateExecutor createExecutor(String path) throws TemplateNotFoundException, TemplateExecutionException, InvalidTemplateException, IOException {
        TemplateExecutor executor = new TemplateExecutor(Template.create(getTestFileUrl(path)), tmpDir);
        executor.setNonInteractive(true);
        return executor;
    }


    private void assertFile(String path, String expectedContent) throws IOException {
        String fileContent = FileUtils.toString(new File(tmpDir.getPath() + File.separator + path.replace("/", File.separator)));
        Assert.assertEquals(expectedContent.trim(), fileContent.trim());
    }

    private void assertOneFile(String expectedContent) throws IOException {
        assertFileCount(1);
        File[] files = tmpDir.listFiles();
        Assert.assertNotNull(files);
        String fileContent = FileUtils.toString(files[0]);
        Assert.assertEquals(expectedContent.trim(), fileContent.trim());
    }

    private void assertFileCount(int expected) {
        int files = 0;
        LinkedList<File> dirs = new LinkedList<>();
        dirs.add(tmpDir);
        while (!dirs.isEmpty()) {
            File file = dirs.removeFirst();
            if (file.isDirectory()) {
                dirs.addAll(Arrays.asList(Objects.requireNonNull(file.listFiles())));
            } else {
                files++;
            }
        }
        Assert.assertEquals(expected, files);
    }

    private static String getTestFileUrl(@NotNull String name) {
        URL resource = TemplatesExecutorTest.class.getResource(name);
        if( resource != null ) {
            return resource.toString();
        } else {
            return name;
        }
    }
}
