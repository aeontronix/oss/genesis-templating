package com.aeontronix.genesis;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.HashSet;
import java.util.Set;

public class DirectoryResourceLoader implements ResourceLoader {
    private File dir;

    public DirectoryResourceLoader(File dir) {
        this.dir = dir;
    }

    @Override
    public InputStream loadResource(String resourcePath) {
        File f = new File(dir + File.separator + resourcePath.replace("/", File.separator));
        try {
            return new FileInputStream(f);
        } catch (FileNotFoundException e) {
            //
        }
        return null;
    }

    @Override
    public Set<String> listFiles(String resourcePath) {
        HashSet<String> results = new HashSet<>();
        buildFileList(results, "", new File(dir, resourcePath));
        return results;
    }

    public void buildFileList(HashSet<String> results, String basePath, File dir) {
        if (dir != null) {
            File[] files = dir.listFiles();
            if (files != null) {
                for (File file : files) {
                    if (file.isDirectory()) {
                        buildFileList(results, basePath + file.getName() + "/", file);
                    } else {
                        results.add(basePath + file.getName());
                    }
                }
            }
        }
    }
}
