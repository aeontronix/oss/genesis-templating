package com.aeontronix.genesis;

import com.aeontronix.commons.StringUtils;
import com.aeontronix.commons.file.FileUtils;
import com.aeontronix.commons.io.IOUtils;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.io.*;

public class TFile {
    protected String path;
    protected String content;
    protected String ignore;
    @JsonIgnore
    protected File file;
    @JsonIgnore
    protected Template template;
    @JsonProperty
    private Boolean process;
    @JsonProperty
    private String encoding;
    @JsonProperty
    private String resource;

    public TFile() {
    }

    public TFile(String path) {
        this.path = path;
    }

    public void create(TemplateExecutor exec) throws TemplateExecutionException {
        if (!Boolean.parseBoolean(ignore)) {
            try {
                File parent = file.getParentFile();
                if (!parent.exists()) {
                    FileUtils.mkdirs(parent);
                }
                try (FileOutputStream os = new FileOutputStream(file); InputStream is = getContent(exec)) {
                    IOUtils.copy(is, os);
                }
            } catch (IOException e) {
                throw new TemplateExecutionException(e);
            }
        }
    }

    private InputStream getContent(TemplateExecutor exec) throws TemplateExecutionException {
        try {
            if (content == null && resource == null) {
                resource = path;
            }
            if (StringUtils.isNotBlank(resource)) {
                String resourcePath = exec.filter(resource);
                InputStream is = template.getFileResource(resourcePath);
                if (is == null) {
                    throw new TemplateExecutionException("File resource missing: " + resourcePath);
                }
                content = IOUtils.toString(is, getEncoding());
            }
            if (content == null) {
                throw new TemplateExecutionException("Content missing: " + path);
            }
            if (process == null || process) {
                return new ByteArrayInputStream(exec.filter(path, content).getBytes(getEncoding()));
            } else {
                return new ByteArrayInputStream(content.getBytes(getEncoding()));
            }
        } catch (IOException e) {
            throw new TemplateExecutionException(e);
        }
    }

    public String getEncoding() {
        return encoding != null ? encoding : "UTF-8";
    }


    public void setTemplate(Template template) {
        this.template = template;
    }

    public boolean isConflict() {
        return file.exists();
    }

    public void process(TemplateExecutor exec) throws TemplateExecutionException {
        this.template = exec.getTemplate();
        path = exec.filter(path);
        file = new File(exec.getTarget() + File.separator + path);
        ignore = exec.filter(ignore);
        encoding = exec.filter(encoding);
        resource = exec.filter(resource);
    }

    public Boolean getProcess() {
        return process;
    }

    public void setProcess(Boolean process) {
        this.process = process;
    }

    public void setEncoding(String encoding) {
        this.encoding = encoding;
    }

    public String getResource() {
        return resource;
    }

    public void setResource(String resource) {
        this.resource = resource;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getIgnore() {
        return ignore;
    }

    public void setIgnore(String ignore) {
        this.ignore = ignore;
    }

}
