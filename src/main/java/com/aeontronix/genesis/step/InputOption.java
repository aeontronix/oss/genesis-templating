package com.aeontronix.genesis.step;

import com.fasterxml.jackson.annotation.JsonProperty;

public class InputOption {
    private String id;
    private String text;

    @JsonProperty(required = true)
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty(required = true)
    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }
}
