package com.aeontronix.genesis.step;

import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.DatabindContext;
import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.jsontype.TypeIdResolver;
import com.fasterxml.jackson.databind.jsontype.impl.TypeIdResolverBase;
import com.fasterxml.jackson.databind.type.TypeFactory;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class StepTypeIdResolver implements TypeIdResolver {
    private final Map<String, Class<? extends Step>> mappings = new HashMap<>();
    private final Map<Class<? extends Step>, String> rev = new HashMap<>();
    private JavaType mBaseType;
    private static HashMap<String,Class<? extends Step>> additionalSteps = new HashMap<>();

    public static void registerStep(String name, Class<? extends Step> stepClass) {
        additionalSteps.put(name,stepClass);
    }

    public StepTypeIdResolver() {
        mappings.put("input", Input.class);
        mappings.put("conditional", ConditionalSteps.class);
        mappings.put("files", FilesStep.class);
        mappings.putAll(additionalSteps);
    }

    @Override
    public void init(JavaType baseType)
    {
        mBaseType = baseType;
    }

    @Override
    public String idFromValue(Object o) {
        return rev.get(o.getClass());
    }

    @Override
    public String idFromValueAndType(Object o, Class<?> aClass) {
        return idFromValue(o);
    }

    @Override
    public String idFromBaseType() {
        return idFromValueAndType(null, mBaseType.getRawClass());
    }

    @Override
    public String getDescForKnownTypeIds() {
        return null;
    }

    @Override
    public JavaType typeFromId(DatabindContext context, String id) throws IOException {
        Class<? extends Step> cl = mappings.get(id);
        if(cl!=null) {
            return TypeFactory.defaultInstance().constructSpecializedType(mBaseType, cl);
        } else {
            return TypeFactory.unknownType();
        }
    }

    @Override
    public JsonTypeInfo.Id getMechanism() {
        return JsonTypeInfo.Id.CUSTOM;
    }
}
