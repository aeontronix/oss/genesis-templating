package com.aeontronix.genesis.step;

import com.aeontronix.genesis.TFile;
import com.aeontronix.genesis.TemplateExecutionException;
import com.aeontronix.genesis.TemplateExecutor;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Set;
import java.util.regex.Pattern;

public class FilesStep extends Step {
    @JsonProperty
    private List<TFile> files;

    public FilesStep() {
    }

    public FilesStep(List<TFile> files) {
        this.files = files;
    }

    @Override
    public void execute(TemplateExecutor exec) throws TemplateExecutionException {
        if (files != null && !files.isEmpty()) {
            for (TFile file : files) {
                file.process(exec);
            }
            for (TFile file : files) {
                if (!matches(exec.getIgnore(), file.getPath())) {
                    file.create(exec);
                }
            }
        }
    }

    private boolean matches(Set<Pattern> ignore, String str) {
        boolean skip = false;
        for (Pattern p : ignore) {
            if (p.matcher(str).matches()) {
                skip = true;
                break;
            }
        }
        return skip;
    }

    private boolean checkConflicts() {
        for (TFile file : files) {
            if (file.isConflict()) {
                return true;
            }
        }
        return false;
    }
}
