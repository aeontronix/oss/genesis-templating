package com.aeontronix.genesis;

import com.aeontronix.commons.exception.UnexpectedException;
import com.aeontronix.genesis.step.Step;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.*;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.List;

public class Template {
    private static final Logger logger = LoggerFactory.getLogger(Template.class);
    private String id;
    private String title;
    private String resourcePath = "files";
    private List<Step> steps;
    private List<TFile> files;
    private boolean overwrite;
    private List<String> ignore;
    private ResourceLoader resourceLoader;

    public Template() {
    }

    Template(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    @JsonProperty
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @JsonProperty(required = true)
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    @JsonProperty
    public List<String> getIgnore() {
        return ignore;
    }

    public void setIgnore(List<String> ignore) {
        this.ignore = ignore;
    }

    public List<TFile> getFiles() {
        return files;
    }

    public void setFiles(List<TFile> files) {
        this.files = files;
    }

    @JsonProperty
    public String getResourcePath() {
        return resourcePath;
    }

    public void setResourcePath(String resourcePath) {
        this.resourcePath = resourcePath;
    }

    public static Template createFromClasspath(String dir, String filename) throws TemplateNotFoundException, InvalidTemplateException, IOException {
        final URL url = Template.class.getResource(dir + "/" + filename);
        if (url == null) {
            throw new TemplateNotFoundException("Template not found: " + url);
        }
        final Template template = create(url.toString());
        if (template == null) {
            throw new TemplateNotFoundException("Template not found: " + url);
        }
        template.setResourceLoader(new ClasspathResourceLoader());
        template.setResourcePath(dir);
        return template;
    }

    public static Template create(String templatePath) throws TemplateNotFoundException, InvalidTemplateException, IOException {
        Template template;
        String lpath = templatePath.toLowerCase();
        File file = null;
        if (lpath.equals("classpath://")) {
            return loadTemplate(new ClasspathResourceLoader());
        }
        try {
            URL url = new URL(templatePath);
            if (url.getProtocol().equalsIgnoreCase("file")) {
                file = new File(url.toURI());
            } else {
                if (lpath.endsWith(".json")) {
                    return loadTemplateFile(url, false);
                } else if (lpath.endsWith(".yml")) {
                    return loadTemplateFile(url, true);
                } else if (lpath.endsWith(".jar") || lpath.endsWith(".zip")) {
                    throw new RuntimeException("non-file url not supported at tis time");
                }
            }
        } catch (MalformedURLException e) {
            file = new File(templatePath);
        } catch (URISyntaxException e) {
            throw new UnexpectedException(e);
        }
        if (file != null && file.exists()) {
            if (file.isDirectory()) {
                return loadTemplate(new DirectoryResourceLoader(file));
            } else {
                final DirectoryResourceLoader resourceLoader = new DirectoryResourceLoader(file.getParentFile());
                if (lpath.endsWith(".json")) {
                    return loadTemplateFile(file, false, resourceLoader);
                } else if (lpath.endsWith(".yml")) {
                    return loadTemplateFile(file, true, resourceLoader);
                } else if (lpath.endsWith(".jar") || lpath.endsWith(".zip")) {
                    return loadTemplate(new ZipResourceLoader(file));
                }
            }
        } else {
            throw new TemplateNotFoundException("Template " + templatePath + " not found");
        }
        return null;
    }

    public static Template loadTemplate(ResourceLoader resourceLoader) throws IOException, InvalidTemplateException {
        try (InputStream is = resourceLoader.loadResource("genesis-template.json")) {
            if (is != null) {
                Template template = loadTemplateFile(is, false, resourceLoader);
                return template;
            }
        }
        try (InputStream is = resourceLoader.loadResource("genesis-template.yml")) {
            if (is == null) {
                throw new InvalidTemplateException("Unable to find template file in archive");
            }
            Template template = loadTemplateFile(is, true, resourceLoader);
            return template;
        }
    }

    private static Template loadTemplateFile(InputStream is, boolean yaml, ResourceLoader resourceLoader) throws IOException {
        ObjectMapper objectMapper = getObjectMapper(yaml);
        final Template template = objectMapper.readValue(is, Template.class);
        template.setResourceLoader(resourceLoader);
        return template;
    }

    @NotNull
    private static ObjectMapper getObjectMapper(boolean yaml) {
        if (yaml) {
            return new ObjectMapper(new YAMLFactory());
        } else {
            return new ObjectMapper();
        }
    }

    private static Template loadTemplateFile(URL url, boolean yaml) throws IOException {
        ObjectMapper objectMapper = getObjectMapper(yaml);
        return objectMapper.readValue(url, Template.class);
    }

    private static Template loadTemplateFile(File file, boolean yaml, ResourceLoader resourceLoader) throws InvalidTemplateException {
        try {
            try (FileInputStream is = new FileInputStream(file)) {
                return loadTemplateFile(is, yaml, resourceLoader);
            }
        } catch (IOException e) {
            throw new InvalidTemplateException(e);
        }
    }

    private static InputStream getStream(String path) {
        File file = new File(path);
        if (file.exists()) {
            try {
                return new FileInputStream(file);
            } catch (FileNotFoundException e) {
                //
            }
        }
        InputStream is = ClassLoader.getSystemResourceAsStream(path);
        if (is != null) {
            return is;
        }
        is = Template.class.getResourceAsStream((path.startsWith("/") ? "" : "/") + path);
        return is;
    }

    public List<Step> getSteps() {
        return steps;
    }

    public void setSteps(List<Step> steps) {
        this.steps = steps;
    }

    public boolean isOverwrite() {
        return overwrite;
    }

    public void setOverwrite(boolean overwrite) {
        this.overwrite = overwrite;
    }

    public ResourceLoader getResourceLoader() {
        return resourceLoader;
    }

    public void setResourceLoader(ResourceLoader resourceLoader) {
        this.resourceLoader = resourceLoader;
    }

    public InputStream getFileResource(String path) throws IOException {
        StringBuilder fullPath = new StringBuilder(resourcePath.startsWith("/") ? resourcePath.substring(1) : resourcePath);
        if (resourcePath.endsWith("/") && path.startsWith("/")) {
            path = path.substring(1);
        } else if (!resourcePath.endsWith("/") && !path.startsWith("/")) {
            path = "/" + path;
        }
        fullPath.append(path);
        return resourceLoader.loadResource(fullPath.toString());
    }
}
