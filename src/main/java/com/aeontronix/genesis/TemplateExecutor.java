package com.aeontronix.genesis;

import com.aeontronix.commons.file.FileUtils;
import com.aeontronix.commons.file.TempDir;
import com.aeontronix.genesis.step.FilesStep;
import com.aeontronix.genesis.step.Step;
import com.aeontronix.genesis.ui.UserInterface;
import com.aeontronix.genesis.ui.UserInterfaceCLIImpl;
import freemarker.cache.StringTemplateLoader;
import freemarker.template.Configuration;
import freemarker.template.TemplateException;
import freemarker.template.TemplateMethodModelEx;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.util.*;
import java.util.regex.Pattern;

public class TemplateExecutor {
    private static final Logger logger = LoggerFactory.getLogger(TemplateExecutor.class);
    private final Configuration fmCfg;
    private Template template;
    private File target;
    private List<Step> steps;
    private final Map<String, String> variables = new HashMap<>();
    private final Map<String, String> defaultOverrides = new HashMap<>();
    private final Map<String, String> defaults = new HashMap<>();
    private boolean nonInteractive;
    private boolean isHeadless;
    private boolean advanced;
    private Set<Pattern> ignore;
    private UserInterface ui;

    public TemplateExecutor(Template template, File target) {
        this.template = template;
        this.target = target;
        fmCfg = new Configuration(Configuration.VERSION_2_3_28);
        fmCfg.setDefaultEncoding("UTF-8");
        fmCfg.setLogTemplateExceptions(false);
        fmCfg.setWrapUncheckedExceptions(true);
        ui = new UserInterfaceCLIImpl();
    }

    public Set<Pattern> getIgnore() {
        return ignore;
    }

    public Template getTemplate() {
        return template;
    }

    public File getTarget() {
        return target;
    }

    public UserInterface getUi() {
        return ui;
    }

    public void setUi(UserInterface ui) {
        this.ui = ui;
    }

    public synchronized String filter(String text) throws TemplateExecutionException {
        return filter("template", text);
    }

    public synchronized String filter(String templateName, String text) throws TemplateExecutionException {
        if (text == null) {
            return null;
        }
        try {
            Map<String, Object> vars = new HashMap<>();
            vars.putAll(defaults);
            vars.putAll(defaultOverrides);
            vars.putAll(variables);
            vars.put("uuid", (TemplateMethodModelEx) (list) -> UUID.randomUUID());
            StringTemplateLoader templateLoader = new StringTemplateLoader();
            templateLoader.putTemplate(templateName, text);
            fmCfg.setTemplateLoader(templateLoader);
            StringWriter buf = new StringWriter();
            fmCfg.getTemplate(templateName).process(vars, buf);
            return buf.toString();
        } catch (TemplateException | IOException e) {
            throw new TemplateExecutionException("An error occurred while processing template: " + text, e);
        }
    }

    public synchronized void execute() throws TemplateExecutionException {
        File originalTarget = null;
        try {
            steps = template.getSteps();
            if( steps == null ) {
                steps = new ArrayList<>();
            }
            logger.debug("Generating template to " + target);
            if (!target.exists()) {
                if (!target.mkdirs()) {
                    throw new TemplateExecutionException("Unable to create directory " + target);
                }
            } else if (!target.isDirectory()) {
                throw new TemplateExecutionException("Target is not a directory " + target);
            } else {
                File[] existingFiles = target.listFiles();
                if (existingFiles != null && existingFiles.length > 0) {
                    originalTarget = target;
                    target = new TempDir("gentempout");
                }
            }
            if (target == null) {
                throw new IllegalArgumentException("Target not set");
            }
            final List<TFile> templateFiles = template.getFiles();
            if( templateFiles != null && !templateFiles.isEmpty() ) {
                steps.add(new FilesStep(templateFiles));
            }
            defaults.clear();
            ignore = new HashSet<>();
            if (template.getIgnore() != null) {
                for (String i : template.getIgnore()) {
                    ignore.add(Pattern.compile(i));
                }
            }
            if (steps != null) {
                for (Step step : steps) {
                    step.execute(this);
                }
            }
            if (originalTarget != null) {
                File[] files1 = originalTarget.listFiles();
                if (files1 != null) {
                    for (File file : files1) {
                        FileUtils.delete(file);
                    }
                }
                FileUtils.copyDirectory(target, originalTarget);
            }
        } catch (IOException e) {
            throw new TemplateExecutionException(e);
        } finally {
            if (originalTarget != null) {
                ((TempDir) target).close();
            }
        }
    }

    public String resolveVariable(String id) {
        if (variables.containsKey(id)) {
            return variables.get(id);
        } else if (defaultOverrides.containsKey(id)) {
            return defaultOverrides.get(id);
        } else {
            return defaults.get(id);
        }
    }

    public Map<String, String> getVariables() {
        return variables;
    }

    public void setVariables(Map<String, String> variables) {
        this.variables.clear();
        if (variables != null) {
            addVariables(variables);
        }
    }

    public String getDefaultValue(String key) {
        return defaultOverrides.get(key);
    }

    public Map<String, String> getDefaultOverrides() {
        return defaultOverrides;
    }

    public void setDefaultOverrides(Map<String, String> defaultOverrides) {
        this.defaultOverrides.clear();
        addDefault(defaultOverrides);
    }

    public void addDefault(Map<String, String> defaults) {
        this.defaultOverrides.putAll(defaults);
    }

    public Map<String, String> getDefaults() {
        return defaults;
    }

    public void setVariable(String id, String val) {
        variables.put(id, val);
    }

    public String getVariable(String id) {
        return variables.get(id);
    }

    public void addVariables(Map<String, String> vars) {
        variables.putAll(vars);
    }

    public boolean containsVariable(String id) {
        return variables.containsKey(id);
    }

    public boolean isNonInteractive() {
        return nonInteractive;
    }

    public void setNonInteractive(boolean nonInteractive) {
        this.nonInteractive = nonInteractive;
    }

    public boolean isHeadless() {
        return isHeadless;
    }

    public void setHeadless(boolean headless) {
        isHeadless = headless;
    }

    public boolean isAdvanced() {
        return advanced;
    }

    public void setAdvanced(boolean advanced) {
        this.advanced = advanced;
    }
}
