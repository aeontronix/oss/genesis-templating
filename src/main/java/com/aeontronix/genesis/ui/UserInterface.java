package com.aeontronix.genesis.ui;

import com.aeontronix.genesis.step.InputOption;

import java.util.List;

public interface UserInterface {
    String requestString(String message, String defaultValue, List<InputOption> options);

}
