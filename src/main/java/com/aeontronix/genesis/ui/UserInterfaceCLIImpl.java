package com.aeontronix.genesis.ui;

import com.aeontronix.commons.ConsoleUtils;
import com.aeontronix.commons.StringUtils;
import com.aeontronix.genesis.step.InputOption;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class UserInterfaceCLIImpl implements UserInterface {
    @Override
    public String requestString(String message, String defaultValue, List<InputOption> options) {
        final List<String> optionsStr = (options != null && !options.isEmpty()) ? options.stream().map(InputOption::getId).collect(Collectors.toList()) : null;
        for (; ; ) {
            System.out.println(message);
            if (defaultValue != null) {
                System.out.println("Default Value: " + defaultValue);
            }
            if (optionsStr != null) {
                System.out.println("Options: " + String.join(", ", optionsStr));
            }
            System.out.print("> ");
            String value = ConsoleUtils.readLine();
            if (StringUtils.isBlank(value) && defaultValue != null) {
                value = defaultValue;
            }
            if (optionsStr != null) {
                if (!optionsStr.contains(value)) {
                    System.out.println("Invalid, must match options");
                    continue;
                }
            }
            return value;
        }
    }
}
