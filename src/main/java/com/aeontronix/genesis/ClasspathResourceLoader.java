package com.aeontronix.genesis;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Set;

public class ClasspathResourceLoader implements ResourceLoader {
    public ClasspathResourceLoader() {
    }

    @Override
    public InputStream loadResource(String resourcePath) throws IOException {
        InputStream is = ClassLoader.getSystemResourceAsStream(resourcePath);
        if( is == null ) {
            is = getClass().getResourceAsStream(resourcePath);
        }
        return is;
    }

    @Override
    public Set<String> listFiles(String resourcePath) throws IOException {
        return Collections.emptySet();
    }
}
